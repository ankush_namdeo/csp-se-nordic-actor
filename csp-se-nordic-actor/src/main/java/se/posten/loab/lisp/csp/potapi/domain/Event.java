/*
 * NPS Event API
 * NPS event definition. Used to report on sorting progress. 
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package se.posten.loab.lisp.csp.potapi.domain;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.threeten.bp.OffsetDateTime;
import se.posten.loab.lisp.csp.potapi.domain.EventEventObjects;
import se.posten.loab.lisp.csp.potapi.domain.EventPayload;

/**
 * A control point event submitted at the device. Basis for track and trace information.
 */
@ApiModel(description = "A control point event submitted at the device. Basis for track and trace information.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-28T15:45:55.354+02:00")
public class Event {
  @SerializedName("event_id")
  private UUID eventId = null;

  @SerializedName("created")
  private OffsetDateTime created = null;

  @SerializedName("device_id")
  private UUID deviceId = null;

  @SerializedName("event_objects")
  private List<EventEventObjects> eventObjects = null;

  @SerializedName("payload")
  private EventPayload payload = null;

  public Event eventId(UUID eventId) {
    this.eventId = eventId;
    return this;
  }

   /**
   * Id of event. Generated by device.
   * @return eventId
  **/
  @ApiModelProperty(required = true, value = "Id of event. Generated by device.")
  public UUID getEventId() {
    return eventId;
  }

  public void setEventId(UUID eventId) {
    this.eventId = eventId;
  }

  public Event created(OffsetDateTime created) {
    this.created = created;
    return this;
  }

   /**
   * Time stamp when the event occurred.
   * @return created
  **/
  @ApiModelProperty(required = true, value = "Time stamp when the event occurred.")
  public OffsetDateTime getCreated() {
    return created;
  }

  public void setCreated(OffsetDateTime created) {
    this.created = created;
  }

  public Event deviceId(UUID deviceId) {
    this.deviceId = deviceId;
    return this;
  }

   /**
   * The device that generated the event.
   * @return deviceId
  **/
  @ApiModelProperty(required = true, value = "The device that generated the event.")
  public UUID getDeviceId() {
    return deviceId;
  }

  public void setDeviceId(UUID deviceId) {
    this.deviceId = deviceId;
  }

  public Event eventObjects(List<EventEventObjects> eventObjects) {
    this.eventObjects = eventObjects;
    return this;
  }

  public Event addEventObjectsItem(EventEventObjects eventObjectsItem) {
    if (this.eventObjects == null) {
      this.eventObjects = new ArrayList<EventEventObjects>();
    }
    this.eventObjects.add(eventObjectsItem);
    return this;
  }

   /**
   * The items related to this event. This is provided as a list of pair (item, barcode) to make events self-contained. In many use cases we need the barcode.
   * @return eventObjects
  **/
  @ApiModelProperty(value = "The items related to this event. This is provided as a list of pair (item, barcode) to make events self-contained. In many use cases we need the barcode.")
  public List<EventEventObjects> getEventObjects() {
    return eventObjects;
  }

  public void setEventObjects(List<EventEventObjects> eventObjects) {
    this.eventObjects = eventObjects;
  }

  public Event payload(EventPayload payload) {
    this.payload = payload;
    return this;
  }

   /**
   * Get payload
   * @return payload
  **/
  @ApiModelProperty(required = true, value = "")
  public EventPayload getPayload() {
    return payload;
  }

  public void setPayload(EventPayload payload) {
    this.payload = payload;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Event event = (Event) o;
    return Objects.equals(this.eventId, event.eventId) &&
        Objects.equals(this.created, event.created) &&
        Objects.equals(this.deviceId, event.deviceId) &&
        Objects.equals(this.eventObjects, event.eventObjects) &&
        Objects.equals(this.payload, event.payload);
  }

  @Override
  public int hashCode() {
    return Objects.hash(eventId, created, deviceId, eventObjects, payload);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Event {\n");
    
    sb.append("    eventId: ").append(toIndentedString(eventId)).append("\n");
    sb.append("    created: ").append(toIndentedString(created)).append("\n");
    sb.append("    deviceId: ").append(toIndentedString(deviceId)).append("\n");
    sb.append("    eventObjects: ").append(toIndentedString(eventObjects)).append("\n");
    sb.append("    payload: ").append(toIndentedString(payload)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

