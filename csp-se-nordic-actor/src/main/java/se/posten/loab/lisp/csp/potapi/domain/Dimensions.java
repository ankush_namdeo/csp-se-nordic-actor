/*
 * NPS Event API
 * NPS event definition. Used to report on sorting progress. 
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package se.posten.loab.lisp.csp.potapi.domain;

import java.util.Objects;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Physical dimensions of an item
 */
@ApiModel(description = "Physical dimensions of an item")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-05-28T15:45:55.354+02:00")
public class Dimensions {
  @SerializedName("height")
  private Integer height = null;

  @SerializedName("width")
  private Integer width = null;

  @SerializedName("depth")
  private Integer depth = null;

  public Dimensions height(Integer height) {
    this.height = height;
    return this;
  }

   /**
   * Dimension 1 (height) of item, in mm
   * @return height
  **/
  @ApiModelProperty(required = true, value = "Dimension 1 (height) of item, in mm")
  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public Dimensions width(Integer width) {
    this.width = width;
    return this;
  }

   /**
   * Dimension 2 (width) of item, in mm
   * @return width
  **/
  @ApiModelProperty(required = true, value = "Dimension 2 (width) of item, in mm")
  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Dimensions depth(Integer depth) {
    this.depth = depth;
    return this;
  }

   /**
   * Dimension 3 (depth) of item, in mm
   * @return depth
  **/
  @ApiModelProperty(required = true, value = "Dimension 3 (depth) of item, in mm")
  public Integer getDepth() {
    return depth;
  }

  public void setDepth(Integer depth) {
    this.depth = depth;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Dimensions dimensions = (Dimensions) o;
    return Objects.equals(this.height, dimensions.height) &&
        Objects.equals(this.width, dimensions.width) &&
        Objects.equals(this.depth, dimensions.depth);
  }

  @Override
  public int hashCode() {
    return Objects.hash(height, width, depth);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Dimensions {\n");
    
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    width: ").append(toIndentedString(width)).append("\n");
    sb.append("    depth: ").append(toIndentedString(depth)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

