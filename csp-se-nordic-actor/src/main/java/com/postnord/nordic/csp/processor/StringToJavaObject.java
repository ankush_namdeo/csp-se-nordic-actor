package com.postnord.nordic.csp.processor;

import org.apache.kafka.streams.kstream.ValueMapper;

import com.google.gson.Gson;

import se.posten.loab.lisp.csp.potapi.domain.Event;

public class StringToJavaObject implements ValueMapper<String, Event> {

	@Override
	public Event apply(String value) {
		Gson gson = new Gson();
		
		return gson.fromJson(value, Event.class);
	}

}
