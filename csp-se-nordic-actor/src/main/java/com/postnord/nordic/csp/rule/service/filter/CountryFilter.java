package com.postnord.nordic.csp.rule.service.filter;
import se.posten.loab.lisp.csp.potapi.domain.Event;

public interface CountryFilter {
    boolean shouldBeClearedInCustoms(Event event);
}
