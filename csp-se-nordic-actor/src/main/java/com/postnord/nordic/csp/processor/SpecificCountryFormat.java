package com.postnord.nordic.csp.processor;


import java.util.Collections;
import java.util.List;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.KeyValueMapper;

import com.google.gson.Gson;

import se.posten.loab.lisp.csp.potapi.domain.Event;

public class SpecificCountryFormat implements KeyValueMapper<String, Event, KeyValue<String, String>> {

	@Override
	public KeyValue<String, String> apply(String key, Event value) {
		Gson gson = new Gson();
		if(value.getPayload()!=null && value.getPayload().getNPS()!=null &&
				value.getPayload().getNPS().getEventN06() !=null && value.getPayload().getNPS().getEventN06().getAdditionalServices() != null){
			List<String> ascList = value.getPayload().getNPS().getEventN06().getAdditionalServices();
			if(ascList.contains("23")) {
				Collections.replaceAll(ascList, "23", "6");
				value.getPayload().getNPS().getEventN06().setAdditionalServices(ascList);
				
				String eventString = gson.toJson(value);
				
		return new KeyValue<>(key, eventString);
			}
			
	}
		return new KeyValue<>(key, gson.toJson(value));
	}
	}
//	@Override
//	public String apply(Event event) {
//		if(event.getPayload()!=null && event.getPayload().getNPS()!=null &&
//				event.getPayload().getNPS().getEventN06() !=null && event.getPayload().getNPS().getEventN06().getAdditionalServices() != null){
//			List<String> ascList = event.getPayload().getNPS().getEventN06().getAdditionalServices();
//			if(ascList.contains("23")) {
//				Collections.replaceAll(ascList, "23", "6");
//				event.getPayload().getNPS().getEventN06().setAdditionalServices(ascList);
//				Gson gson = new Gson();
//				return gson.toJson(event);
//			}
//
//		}
//		Gson gson = new Gson();
//		return gson.toJson(event);
//	}
	

