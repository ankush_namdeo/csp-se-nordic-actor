package com.postnord.nordic.csp.processor;

import java.util.Properties;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KeyValueMapper;
import org.apache.kafka.streams.kstream.Predicate;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.ValueMapper;

import com.postnord.nordic.csp.util.KafkaUtil;
import com.postnord.nordic.csp.util.SystemUtil;

import se.posten.loab.lisp.csp.potapi.domain.Event;


public class Processor {
	private static final String APPLICATION_ID = "csp-se-nordic-actor";

	public void process() {
		KafkaStreams stream = new KafkaStreams(topology(),config());
		stream.start();
	}

	private Topology topology() {
		final Serde<String> stringSerde = Serdes.String();
		StreamsBuilder builder = new StreamsBuilder();
		KStream<String , Event> javaconverted = builder.stream(inputTopic(), consumeResult()).filter(isNull()).mapValues(stringToJava());
		
		KStream<String, Event> filtered = javaconverted.filter(countryFilter());
		
		
		KStream<String, String> mapped = filtered.map(convertToCountryFormat());
		
		mapped.to(outputTopic(),Produced.with(stringSerde, stringSerde));
		return builder.build();
	 
	}
	private KeyValueMapper<String, Event ,KeyValue<String,String>> convertToCountryFormat() {
		// TODO Auto-generated method stub
		return new SpecificCountryFormat();
	}

	private Produced<String, String> produceResult() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();

		return Produced.with(keySerde, valueSerde);
	}

	

	private String inputTopic() {
		System.out.println("inset loop");
		return SystemUtil.getEnvironmentVariable("INPUT_TOPIC", "test3");
	}
	private String outputTopic() {
		return SystemUtil.getEnvironmentVariable("OUTPUT_TOPIC", "test-consumer");
	}

//	private Predicate<String,Event> countryFilter() {
//		CountryFilter filter = new OriginCountryFilter();
//		return new TransactionShouldBeClearedInCustoms(filter);	
//	}
	
	
	private Predicate<String,Event> countryFilter(){
		
		return (key , event) -> ((null != event.getPayload().getNPS() && null != event.getPayload().getNPS().getEventN06() && event.getPayload().getNPS().getEventN06().getIssuerCode().equalsIgnoreCase("DK")));
	}

	private ValueMapper<String, Event> stringToJava() {
		return new StringToJavaObject();
	}

	private Predicate<String, String> isNull() {
		return (k, v) -> v != null;
	}

	private Consumed<String, String> consumeResult() {
		Serde<String> keySerde = Serdes.String();
		Serde<String> valueSerde = Serdes.String();
		return Consumed.with(keySerde, valueSerde);
	}
	
	private Properties config() {
		return KafkaUtil.getStreamsApplicationProperties(APPLICATION_ID);
	}
	



}
