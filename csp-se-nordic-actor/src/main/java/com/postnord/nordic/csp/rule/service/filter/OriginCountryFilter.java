package com.postnord.nordic.csp.rule.service.filter;


import se.posten.loab.lisp.csp.potapi.domain.Event;
import se.posten.loab.lisp.csp.potapi.domain.EventPayloadHCP;
import se.posten.loab.lisp.csp.potapi.domain.EventPayloadNPS;


public class OriginCountryFilter implements CountryFilter {
	public OriginCountryFilter() {}

	@Override
	public boolean shouldBeClearedInCustoms(Event event) {
		EventPayloadNPS eventPayloadNPS = event.getPayload().getNPS();
		EventPayloadHCP eventPayloadHCP = event.getPayload().getHCP();
		if(eventPayloadNPS != null) {
			if(eventPayloadNPS.getEventCKC() != null) {
				//db query required
				if(eventPayloadNPS.getEventCKC().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
			if(eventPayloadNPS.getEventN06() != null) {
				// Discard for Issuer_coude and Recipient_country_code is Sweden
				if(eventPayloadNPS.getEventN06().getIssuerCode().equalsIgnoreCase("SE") && eventPayloadNPS.getEventN06().getRecipientCountry().equalsIgnoreCase("SE")) {
					return true;
				}
				return false;
			}
			if(eventPayloadNPS.getEventNRT() != null) {
				//db query required
				return true;
			}
			if(eventPayloadNPS.getEventN89() != null) {
				//db query required
				return true;
			}
		}


		if(eventPayloadHCP != null) {
			if(eventPayloadHCP.getControlpointC12() != null) {
				//db query required
				if(eventPayloadHCP.getControlpointC12().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
			if(eventPayloadHCP.getControlpointC13() != null) {
				//db query required
				if(eventPayloadHCP.getControlpointC13().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
			if(eventPayloadHCP.getControlpointC16() != null) {
				//db query required
				if(eventPayloadHCP.getControlpointC16().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
			if(eventPayloadHCP.getControlpointC19() != null) {
				//db query required
				if(eventPayloadHCP.getControlpointC19().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
			if(eventPayloadHCP.getControlpointC20() != null) {
				if(eventPayloadHCP.getControlpointC20().getLocationId().equalsIgnoreCase("SE")) {
					return false;
				}
				return true;
			}
		}
		return true;
	} 


}
