package com.postnord.nordic.csp.processor.internal;

import org.apache.kafka.streams.kstream.Predicate;

import com.postnord.nordic.csp.rule.service.filter.CountryFilter;

import se.posten.loab.lisp.csp.potapi.domain.Event;

public class TransactionShouldBeClearedInCustoms implements Predicate<String, Event> {
    private CountryFilter countryFilter;

    public TransactionShouldBeClearedInCustoms(CountryFilter countryFilter) {
        this.countryFilter = countryFilter;
    }

    @Override
    public boolean test(String unusedKey, Event value) {
        return countryFilter.shouldBeClearedInCustoms(value);
    }
}
