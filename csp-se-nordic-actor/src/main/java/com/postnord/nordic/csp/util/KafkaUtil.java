package com.postnord.nordic.csp.util;

import java.util.Properties;

import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.streams.StreamsConfig;

public class KafkaUtil {
	  public static Properties getStreamsApplicationProperties(String defaultApplicationId) {
	        int requestSize = 20 * 1024 * 1024; // 20 megabyte

	        Properties config = new Properties();

	        config.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, KafkaUtil.getKafkaBootstrapServers());
	        config.put(CommonClientConfigs.CONNECTIONS_MAX_IDLE_MS_CONFIG, 7 * 60 * 1000);

	        config.put(StreamsConfig.APPLICATION_ID_CONFIG, getStreamsApplicationId(defaultApplicationId));
	        config.put(StreamsConfig.CLIENT_ID_CONFIG, getStreamsClientId(defaultApplicationId));
	       
	        return config;
	  }
	  private static String getKafkaBootstrapServers() {
		  //specifies the broker to use
	        String defaultHosts = "kafka-1.orm-dev-service:9092,kafka-2.orm-dev-service:9092,kafka-3.orm-dev-service:9092";

	        return defaultHosts;
	    }
	  private static String getStreamsApplicationId(String defaultApplicationId) {
	        return "orm";
	    }

	    private static String getStreamsClientId(String defaultApplicationId) {
	        return "csp";
	    }
}

